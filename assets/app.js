// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.scss";
import "tw-elements";
import "@fortawesome/fontawesome-free/js/all.js";
import "@fortawesome/fontawesome-free/css/all.css";
import "./bootstrap";
import "./scripts/scrollToTop";
import "./styles/scrollToTop.scss";
